#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 16 12:29:49 2018
@title: Simulazione camera propellente solido a geometria cilindrica
@author: fpasqua
"""
import numpy as np
from numpy import pi,sqrt,power
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

# Parametri Iniziali
P_ATM = 1.01e5      # Pressione atmosferica / all'inizio del processo [Pa]
H = 0.05            # Lunghezza propellente e camera [m]
A = 2e-6            # Coefficiente moltiplicativo del Burning Rate
B = 0.5253          # Indice di combustione del Burning Rate
RHO_P = 1900        # Densità propellente solido [kg/m3]
R_SPEC = 368        # Costante dei gas specifica del gas di scarico
T_C = 2300          # Temperatura di Combustione (di Fiamma) [K]
GAMMA = 1.2336      # Gamma del gas di scarico
R_T = 0.0035        # Raggio del throat [m]

# Parametri di simulatore
DT = 1e-4                       # Step temporale della simulazione [s]
PLUS_SIMPLY_MODEL = True        # Genera i dati anche per il modello semplificato algebrico
#PREDICTOR_CORRECTOR = True      # Utilizza metodo di Heun di tipo Predittore-Correttore
DATA_FILENAME_IN = "star_chamber_data.out"


# Calcolo parametri iniziali del simulatore
A_T = pi*R_T**2     # Area del throat [m2]
c = sqrt(GAMMA/(R_SPEC*T_C)*power(2/(GAMMA+1),(GAMMA+1)/(GAMMA-1)))
def P_dot(P,S_b,V_c):
    """Derivata istantanea della pressione al prim'ordine"""
    return (R_SPEC*T_C/V_c)*(S_b*RHO_P*(A*P**B) - c*A_T*P)

# Dati finali
P_c = [P_ATM]
if PLUS_SIMPLY_MODEL:
    P_approx = [P_ATM]
else:
    P_approx = None
    
# Input dati di geometria della camera
T, Perim, Surf, = np.loadtxt("star_chamber_data.out", unpack=True)
vel_input = 0.001 #m/s della velocità a cui è calcolato il dato d'ingresso
# T tempo, Perim perimetro, Surf superficie inclusa, tutto a velocità 0.001m/s
S = Perim*H
V = Surf*H

# Stato iniziale t=0
V_c = V[0]          # Volume della camera
S_b = S[0]           # Superficie di combustione
P = P_ATM           # Pressione nella camera (posta uniforme in tutto il volume)
r = A*power(P,B)    # Burning Rate della camera

#salva_S = [S_b]
#salva_V = [V_c]
#salva_r = [r]

while True:
    # Step iterativo della simulazione
    if PLUS_SIMPLY_MODEL:
        P_approx.append(((RHO_P*A*S_b)/(A_T*c))**(1/(1-B)))
    pd = P_dot(P,S_b,V_c)
    # Calcolo della variazione istantanea di pressione
    dP = pd*DT
    rateo_vel = r / vel_input
    tmp_T = T / rateo_vel
    ts_tilde = interp1d(S, tmp_T, bounds_error = False)(S_b)
    tv_tilde = interp1d(V, tmp_T, bounds_error = False)(V_c)
    if np.isnan(ts_tilde) or np.isnan(tv_tilde):
        break
    S_b = interp1d(tmp_T, S, bounds_error = False)(ts_tilde + DT) # Aggiornamento della superficie di combustione
    V_c = interp1d(tmp_T, V, bounds_error = False)(tv_tilde + DT) # Aggiornamento del volume della camera 
    if np.isnan(S_b) or np.isnan(V_c):
        break
    P = P + dP                  # Aggiornamento della Pressione (t_n->t_n+1)
    r = A*P**B              # Aggiornamento del Burning Rate per ciclo successivo
    # Stockaggio risultati
    #salva_S.append(S_b)
    #salva_V.append(V_c)
    #salva_r.append(r)
    P_c.append(P)
P_approx.pop()
time = np.array(range(len(P_c)))*DT
pres = np.array(P_c)
if P_approx is not None:
    pres_ap = np.array(P_approx)
else:
    pres_ap = None
    
t,p,p_ap = time, pres, pres_ap

# Visualizzazione grafica curva di dt-pressione
caption = "Integ. "
caption += "1step "
caption += "Primo Ordine"
print(p)
plt.plot(t, p/1e6,'r-',label=caption)
if PLUS_SIMPLY_MODEL:
    plt.plot(t,p_ap/1e6,'b--',label="Mod. Semplificato")
plt.title("Curva Tempo-Pressione")
plt.xlabel("Tempo [s]")
plt.ylabel("Pressione [MPa]")
plt.legend(loc='best')
plt.show()
