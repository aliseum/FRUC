#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  1 11:03:49 2018

@author: fpasqua
"""
from enum import Enum
import numpy as np
from numpy import pi,sqrt,power
import matplotlib.pyplot as plt

class States(Enum):
    NOT_INITIALIZED = 0
    INITIALIZED = 1
    SIMULATION = 2
    COMPLETED = 3

from States import NOT_INITIALIZED,INITIALIZED,SIMULATION,COMPLETED

class Fruc_Simulation:
    # Any Machine State
    def __init__(self, delta_t, simulate_final_transient = True, second_order_integr = False):
        self.DT = np.float64(delta_t) # Delta_time della simulazione
        self.SIMULATE_FINAL_TRANSIENT = bool(simulate_final_transient) # Flag per la simulazione del transiente di esaurimento
        self.SECOND_ORDER_INTEGR = bool(second_order_integr) # Flag per l'uso dell'integrazione al secondo ordine (ALPHA)
        self.STATE = NOT_INITIALIZED
    # Machine State 0 - NOT_INITIALIZED
    def initialize_geometry(R_0, R_FIN, H, R_T):
        self.GEOMETRY = (R_0, R_FIN, H, R_T)
        if self.PHYSICS is not None:
            self.STATE = INITIALIZED
    def initialize_physics(P_ATM, A, B, RHO_P, R_SPEC, T_C, GAMMA):
        self.PHYSICS = (P_ATM, A, B, RHO_P, R_SPEC, T_C, GAMMA)
        if self.GEOMETRY is not None:
            self.STATE = INITIALIZED
    def initialize_global(P_ATM, R_0, R_FIN, H, A, B, RHO_P, R_SPEC, T_C, GAMMA, R_T):
        self.initialize_geometry(R_0, R_FIN, H, R_T)
        self.initialize_physics(P_ATM, A, B, RHO_P, R_SPEC, T_C, GAMMA)
    # Machine State 1 - INITIALIZED