Created on Mon Jul 16 17:33:20 2018
Dati salvati
@author: aliseum

# Dati1: Shock con Plateau 
P_ATM = 1.01e5      # Pressione atmosferica / all'inizio del processo [Pa]
R_0 = 0.02          # Raggio interno iniziale del propellente [m]
R_FIN = 0.03        # Raggio esterno del propellente [m]
H = 0.12            # Lunghezza propellente e camera [m]
A = 2e-6            # Coefficiente moltiplicativo del Burning Rate
B = 0.5253          # Indice di combustione del Burning Rate
RHO_P = 1900        # Densità propellente solido [kg/m3]
R_SPEC = 368        # Costante dei gas specifica del gas di scarico
T_C = 2350          # Temperatura di Combustione (di Fiamma) [K]
GAMMA = 1.2336      # Gamma del gas di scarico
R_T = 0.00533       # Raggio del throat [m]

# Dati2: Plateau con meno shock che brucia di piu
P_ATM = 1.01e5      # Pressione atmosferica / all'inizio del processo [Pa]
R_0 = 0.015          # Raggio interno iniziale del propellente [m]
R_FIN = 0.03        # Raggio esterno del propellente [m]
H = 0.12            # Lunghezza propellente e camera [m]
A = 2e-6            # Coefficiente moltiplicativo del Burning Rate
B = 0.5253          # Indice di combustione del Burning Rate
RHO_P = 1900        # Densità propellente solido [kg/m3]
R_SPEC = 368        # Costante dei gas specifica del gas di scarico
T_C = 2350          # Temperatura di Combustione (di Fiamma) [K]
GAMMA = 1.2336      # Gamma del gas di scarico
R_T = 0.00533       # Raggio del throat [m]

