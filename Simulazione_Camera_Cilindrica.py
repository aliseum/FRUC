#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 16 12:29:49 2018
@title: Simulazione camera propellente solido a geometria cilindrica
@author: fpasqua
"""
import numpy as np
from numpy import pi,sqrt,power
import matplotlib.pyplot as plt

# Parametri Iniziali
P_ATM = 1.01e5      # Pressione atmosferica / all'inizio del processo [Pa]
R_0 = 0.02          # Raggio interno iniziale del propellente [m]
R_FIN = 0.025       # Raggio esterno del propellente [m]
H = 0.05            # Lunghezza propellente e camera [m]
A = 2e-6            # Coefficiente moltiplicativo del Burning Rate
B = 0.5253          # Indice di combustione del Burning Rate
RHO_P = 1900        # Densità propellente solido [kg/m3]
R_SPEC = 368        # Costante dei gas specifica del gas di scarico
T_C = 2300          # Temperatura di Combustione (di Fiamma) [K]
GAMMA = 1.2336      # Gamma del gas di scarico
R_T = 0.0035        # Raggio del throat [m]

# Parametri di simulatore
DT = 1e-5                       # Step temporale della simulazione [s]
SIMULATE_FINAL_TRANSIENT = True # Flag per la simulazione del transiente di esaurimento
SECOND_ORDER_INT = False        # Flag per l'uso dell'integrazione al secondo ordine (ALPHA)
PLUS_SIMPLY_MODEL = True        # Genera i dati anche per il modello semplificato algebrico
PREDICTOR_CORRECTOR = True      # Utilizza metodo di Heun di tipo Predittore-Correttore


# Se PLUS_SIMPLY_MODEL = True, allora viene automaticamente posto SIMULATE_FINAL_TRANSIENT = False
if PLUS_SIMPLY_MODEL: SIMULATE_FINAL_TRANSIENT = False
if PREDICTOR_CORRECTOR and SECOND_ORDER_INT: print("WARNING! PREDICTOR_CORRECTOR and SECOND_ORDER_INT flags are not recommended togheter on.")

def simulazione(P_ATM, R_0, R_FIN, H, A, B, RHO_P, R_SPEC, T_C, GAMMA, R_T, DT):
    """
    Simulazione della camera in funzione dei parametri iniziali
    
    """
    # Calcolo parametri iniziali del simulatore
    A_T = pi*R_T**2     # Area del throat [m2]
    S_0 = 2*pi*R_0*H    # Superficie di combustione iniziale [m2]
    V_0 = pi*R_0**2*H   # Volume della camera iniziale [m3]
    c = sqrt(GAMMA/(R_SPEC*T_C)*power(2/(GAMMA+1),(GAMMA+1)/(GAMMA-1)))
    def P_dot(P,S_b,V_c):
        """Derivata istantanea della pressione al prim'ordine"""
        return (R_SPEC*T_C/V_c)*(S_b*RHO_P*(A*P**B) - c*A_T*P)
    def P_dotdot(P,S_b,V_c,P_dot,R):
        """Derivata istantanea della pressione al second'ordine"""
        def f_S_bdot(P):
            return 2*pi*H*(A*P**B)
        def f_V_cdot(P,R):
            return 2*pi*R*H*(A*P**B)
        S_bdot = f_S_bdot(P)
        V_cdot = f_V_cdot(P,R)
        return (R_SPEC*T_C/V_c**2)*((RHO_P*S_bdot*A*P**B + RHO_P*A*S_b*B*P**(B-1)*P_dot - c*A_T*P_dot)*V_c - (S_b*RHO_P*A*P**B - c*A_T*P)*V_cdot)
    
    # Dati finali
    P_c = [P_ATM]
    if PLUS_SIMPLY_MODEL:
        P_approx = [P_ATM]
    else:
        P_approx = None
    
    # Stato iniziale t=0
    R = R_0             # Raggio della camera
    V_c = V_0           # Volume della camera
    S_b = S_0           # Superficie di combustione
    P = P_ATM           # Pressione nella camera (posta uniforme in tutto il volume)
    r = A*power(P,B)    # Burning Rate della camera
    
    # P >= P_ATM <--|--> R < R_FIN
    if SIMULATE_FINAL_TRANSIENT:
        loop_checker = lambda p,r: p >= P_ATM
    else:
        loop_checker = lambda p,r: r < R_FIN
    while loop_checker(P,R):
        # Step iterativo della simulazione
        if PLUS_SIMPLY_MODEL:
            P_approx.append(((RHO_P*A*S_b)/(A_T*c))**(1/(1-B)))
        pd = P_dot(P,S_b,V_c)
        # Calcolo della variazione istantanea di pressione
        if SECOND_ORDER_INT:
            dP = pd*DT + (0.5*P_dotdot(P,S_b,V_c,pd,R))*DT**2    
        else:
            dP = pd*DT 
        if PREDICTOR_CORRECTOR:
            tilde_P = P + dP                  # Stima della Pressione (t_n->tilde_t_n+1)
            if R < R_FIN:
                tilde_R = R + r*DT            # Aggiornamento del raggio della camera
                tilde_V_c = pi*tilde_R**2*H         # Aggiornamento del volume della camera
                tilde_S_b = 2*pi*tilde_R*H          # Aggiornamento della superficie di combustione
                #tilde_r = A*tilde_P**B              # Aggiornamento del Burning Rate per ciclo successivo
            else:
                tilde_R = R
                tilde_V_c = V_c
                tilde_S_b = 0
            # Calcolo dello step implicito
            P = P + DT/2*( P_dot(P,S_b,V_c) + P_dot(tilde_P,tilde_S_b,tilde_V_c) ) # Aggiornamento della Pressione (t_n->t_n+1)
            # Stima finale    
            if R < R_FIN:
                R = R + r*DT            # Aggiornamento del raggio della camera
                V_c = pi*R**2*H         # Aggiornamento del volume della camera
                S_b = 2*pi*R*H          # Aggiornamento della superficie di combustione
                r = A*P**B              # Aggiornamento del Burning Rate per ciclo successivo
            else:
                S_b = 0
        else:
            P = P + dP                  # Aggiornamento della Pressione (t_n->t_n+1)
            if R < R_FIN:
                R = R + r*DT            # Aggiornamento del raggio della camera
                V_c = pi*R**2*H         # Aggiornamento del volume della camera
                S_b = 2*pi*R*H          # Aggiornamento della superficie di combustione
                r = A*P**B              # Aggiornamento del Burning Rate per ciclo successivo
            else:
                S_b = 0
        # Stockaggio risultati
        P_c.append(P)
    time = np.array(range(len(P_c)))*DT
    pres = np.array(P_c)
    if P_approx is not None:
        pres_ap = np.array(P_approx)
    else:
        pres_ap = None
    return time, pres, pres_ap
    
# Funzioni utili per il terminale
def default_run():
    t,p,p_ap = simulazione(P_ATM, R_0, R_FIN, H, A, B, RHO_P, R_SPEC, T_C, GAMMA, R_T, DT)
    # Visualizzazione grafica curva di dt-pressione
    caption = "Integ. "
    if PREDICTOR_CORRECTOR:
        caption += "2step "
    else:
        caption += "1step "
    if SECOND_ORDER_INT:
        caption += "Secondo Ordine"
    else:
        caption += "Primo Ordine"
    plt.plot(t, p/1e6,'r-',label=caption)
    if PLUS_SIMPLY_MODEL:
        plt.plot(t,p_ap/1e6,'b--',label="Mod. Semplificato")
    plt.title("Curva Tempo-Pressione")
    plt.xlabel("Tempo [s]")
    plt.ylabel("Pressione [MPa]")
    plt.legend(loc='best')
    plt.show()
    
def print_parameter():
    print("P_ATM = %f" % P_ATM ) 
    print("R_0 = %f" % R_0 )   
    print("R_FIN = %f" % R_FIN ) 
    print("H = %f" % H )     
    print("A = %f" % A )     
    print("B = %f" % B )     
    print("RHO_P = %f" % RHO_P ) 
    print("R_SPEC = %f" % R_SPEC)
    print("T_C = %f" % T_C )   
    print("GAMMA = %f" % GAMMA )     
    print("R_T = %f" % R_T )

if __name__=='__main__':
    default_run()