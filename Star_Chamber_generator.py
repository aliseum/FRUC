#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 14 18:33:23 2018

@author: fpasqua
"""
import cv2
WHITE = 255
BLACK = 0
import numpy as np
# Per dsiabilitare l'output grafico (show o non show che sia)
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import skfmm 

D_E = 0.05  # Diametro esterno [in metri]
N_CELLE = 5000 # Numero di celle (deve essere un numero intero)

N = 5 # Numero di punte della stella
R_I = 0.005 # Raggio del cerchio interno di vuoto
L_S = 0.01 # Lunghezza punta dal centro senza testa curva
THETA = 0 # Inclinazione dei lati della stella [in gradi]
S_S = 0.004 # Spessore della punta, e raggio della punta esterna

N_CON = 100 # Numero di contorni impiegati (sempre maggiore di 2)

# Inizio programma

DS = D_E/N_CELLE # Lunghezza lato cella [in metri]
print("DS = {0}m".format(DS))

DENSITY = N_CELLE
C = int(DENSITY//2)

R = int(R_I//DS)
L = int(L_S//DS)
S = int(S_S//DS)

# Use Green's theorem to compute the area
# enclosed by the given contour.
def length_and_area(vs):
    a = 0
    p = 0
    x0,y0 = vs[0]
    for x1,y1 in vs[1:]:
        dx = x1-x0
        dy = y1-y0
        a += 0.5*(y0*dx - x0*dy)
        p += np.hypot(dx,dy)
        x0 = x1
        y0 = y1
    x0,y0 = vs[0]
    xf,yf = vs[-1]
    p += np.hypot(x0-xf,y0-yf)
    return p,a

img = np.zeros((DENSITY,DENSITY), np.uint8)

#img = cv2.rectangle(img, (DENSITY//2-S//2,DENSITY//2), (DENSITY//2+S//2,DENSITY//2+L), 255, cv2.FILLED )
pts = [(C+S//2,C+L), (C-S//2,C+L), (C-S//2-L*np.tan(np.deg2rad(THETA)),C), (C+S//2+L*np.tan(np.deg2rad(THETA)),C)]
img = cv2.fillPoly(img, [np.array(pts, np.int32)], WHITE)
img = cv2.circle(img, (C,C+L), S//2, WHITE, cv2.FILLED)

rotmat = cv2.getRotationMatrix2D((C,C), 360/N, 1)
tmp = img[:,:]
for i in range(N):
    tmp = cv2.warpAffine(tmp, rotmat, (DENSITY,DENSITY))
    img = cv2.bitwise_or(img,tmp)
del tmp
img = cv2.circle(img, (C,C), R, WHITE, cv2.FILLED)

# Pulitura degli artefatti
kernel = np.ones((7,7),np.uint8)
img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

#cv2.imshow('image_final',img)
#cv2.imwrite('Camera.png', img)
print("Chamber generated")

data = np.array(img,dtype=(np.float64,np.float64))
data[data > 0] = 1
data[data == 0] = -1

X, Y = np.meshgrid(np.arange(DENSITY), np.arange(DENSITY))
mask = ( (X - C)**2 + (Y - C)**2 ) > (DENSITY/2)**2
data = np.ma.MaskedArray(data, mask)

vel = 0.001 # meter/sec
speed = np.ones_like(img)*vel
t = skfmm.travel_time(data, speed, dx=DS) 
print("FMM done")

Z = t #TODO: Rimuovere copia quando si è finito di lavorare
Z[data == 1] = 0

plt.figure()
X,Y = np.meshgrid(np.linspace(-D_E/2,D_E/2,DENSITY), np.linspace(-D_E/2,D_E/2,DENSITY))
cs_0 = plt.contour(X, Y, data, colors='black', levels=[0.])
cs = plt.contour(X, Y, Z, levels=np.linspace(np.min(Z), np.max(Z), N_CON)[:-1])
print("Contour done")
plt.colorbar(cs, shrink=0.8, extend='both')
plt.show()

L = np.concatenate(([0.],cs.levels))
P = np.zeros_like(L)
S = np.zeros_like(L)

p, a = length_and_area(cs_0.collections[0].get_paths()[0].vertices)
#P[0] = None # Viene calcolato successivamente
S[0] = np.abs(a)

for i,con in enumerate(cs.collections):
    if len(con.get_paths()) != 1:
        break
    vs = con.get_paths()[0].vertices
    p,a = length_and_area(vs)
    P[i+1] = p
    S[i+1] = a
print("Generated P and S")
    
L = L[:i+1]
P = P[:i+1]
S = S[:i+1]

# Interpolazione lineare esterna del primo valore
P[0] = P[1] - (P[2] - P[1])/(L[2] - L[1]) * L[1]

plt.figure()
plt.title("Lunghezza della superficie")
plt.plot(L,P,'bo-')
plt.show()
plt.figure()
plt.title("Area interna senza propellente")
plt.plot(L,S,'ro-')
plt.show()

np.savetxt("star_chamber_data.out", np.stack((L, P, S), axis=1), header="Time (s)\tLength (m)\tSurface(m2)")
print("Output completed")

# =============================================================================
# ret, thresh = cv2.threshold(img, 127,255,0)
# _, contours, _ = cv2.findContours(thresh,cv2.RETR_LIST,cv2.CHAIN_APPROX_TC89_KCOS)
# cnt = contours[0]
# print("Perimetro = {}".format(cv2.arcLength(cnt, True)))
# =============================================================================
